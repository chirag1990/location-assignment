﻿using System;
using System.Collections.ObjectModel;
using Windows.Devices.Geolocation;
using StaffordshireMaps.Models;

namespace StaffordshireMaps.Helper
{
    public static class DataStore
    {
        public static ObservableCollection<CustomLocationModel> BuildingLocations;
        public static ObservableCollection<RoomsModel> AllRooms;

        //Getting the building details 
        public static ObservableCollection<CustomLocationModel> GetBuildingLocations()
        {
            if (BuildingLocations != null && BuildingLocations.Count > 0)
                return BuildingLocations;
            BuildingLocations = new ObservableCollection<CustomLocationModel>
            {
                new CustomLocationModel
                {
                    Id = 1,
                    AutomatedAccessPoint = true,
                    IsLiftPresent = true,
                    IsPrinterPresent = true,
                    Name = "Beacon Building",
                    Position = new BasicGeoposition {Latitude =52.682426, Longitude = -1.905730 },
                    SpecCompFacility = true,
                    IconPath = "ms-appx:///Assets/customIcons/WORLD.png"
                },
                new CustomLocationModel
                {
                    Id = 2,
                    AutomatedAccessPoint = true,
                    IsLiftPresent = true,
                    IsPrinterPresent = true,
                    Name = "The Octagon",
                    Position = new BasicGeoposition {Latitude =53.010243, Longitude = -2.182618 },
                    SpecCompFacility = false,
                    IconPath = "ms-appx:///Assets/customIcons/WORLD.png"
                },
                new CustomLocationModel
                {
                    Id = 3,
                    AutomatedAccessPoint = false,
                    IsLiftPresent = true,
                    IsPrinterPresent = false,
                    Name = "Stafford Court",
                    Position = new BasicGeoposition {Latitude =52.810633, Longitude = -2.080117},
                    SpecCompFacility = false,
                    IconPath = "ms-appx:///Assets/customIcons/WORLD.png"
                },
                new CustomLocationModel
                {
                    Id = 4,
                    AutomatedAccessPoint = false,
                    IsLiftPresent = false,
                    IsPrinterPresent = false,
                    Name = "Beaconside Sports Centre",
                    Position = new BasicGeoposition {Latitude =52.812227, Longitude = -2.080864 },
                    SpecCompFacility = false,
                    IconPath = "ms-appx:///Assets/customIcons/WORLD.png"
                },

                new CustomLocationModel
                {
                    Id = 5,
                    AutomatedAccessPoint = true,
                    IsLiftPresent = true,
                    IsPrinterPresent = false,
                    Name = "Centre of Excellence",
                    Position = new BasicGeoposition {Latitude =52.813539, Longitude = -2.072795 },
                    SpecCompFacility = true,
                    IconPath = "ms-appx:///Assets/customIcons/WORLD.png"
                },
                new CustomLocationModel
                {
                    Id = 6,
                    AutomatedAccessPoint = true,
                    IsLiftPresent = true,
                    IsPrinterPresent = true,
                    Name = "Stafford Business Village",
                    Position = new BasicGeoposition {Latitude =52.814950, Longitude = -2.083274 },
                    SpecCompFacility = false,
                    IconPath = "ms-appx:///Assets/customIcons/WORLD.png"
                },

                new CustomLocationModel
                {
                    Id = 7,
                    AutomatedAccessPoint = true,
                    IsLiftPresent = true,
                    IsPrinterPresent = true,
                    Name = "2 Winton Square",
                    Position = new BasicGeoposition {Latitude =53.008306, Longitude = -2.180089 },
                    SpecCompFacility = false,
                    IconPath = "ms-appx:///Assets/customIcons/WORLD.png"
                },
                new CustomLocationModel
                {
                    Id = 8,
                    AutomatedAccessPoint = false,
                    IsLiftPresent = false,
                    IsPrinterPresent = true,
                    Name = "3 Winton Square",
                    Position = new BasicGeoposition {Latitude =53.008133, Longitude = -2.180153 },
                    SpecCompFacility = false,
                    IconPath = "ms-appx:///Assets/customIcons/WORLD.png"
                },
                new CustomLocationModel
                {
                    Id = 9,
                    AutomatedAccessPoint = false,
                    IsLiftPresent = false,
                    IsPrinterPresent = false,
                    Name = "4 Winton Square",
                    Position = new BasicGeoposition {Latitude =53.008133, Longitude = -2.180153 },
                    SpecCompFacility = false,
                    IconPath = "ms-appx:///Assets/customIcons/WORLD.png"
                },
                new CustomLocationModel
                {
                    Id = 10,
                    AutomatedAccessPoint = false,
                    IsLiftPresent = false,
                    IsPrinterPresent = false,
                    Name = "Ashley Center",
                    Position = new BasicGeoposition {Latitude =53.010144, Longitude = -2.174351 },
                    SpecCompFacility = true,
                    IconPath = "ms-appx:///Assets/customIcons/WORLD.png"
                }
            };
            return BuildingLocations;
        }
        
        //Getting the room details. Make sure the Building Id maps to existing buildings
        public static ObservableCollection<RoomsModel> GetRoomsDetails()
        {
            if (AllRooms != null && AllRooms.Count > 0)
                return AllRooms;
            AllRooms = new ObservableCollection<RoomsModel>
            {
                new RoomsModel
                {
                    Name = "Lecture Room 1",
                    BookedBy = "Joshua Taylor",
                    // BookedDate = DateTime.Today,
                    BookedTimeFrom = DateTime.Now.AddHours(1),
                    BookedTimeTill = DateTime.Now.AddHours(3),
                    BookingDate =  DateTime.Today.AddDays(-1),
                    BuildingId = 1,
                    Id = 1
                },
                new RoomsModel
                {
                    Name = "Computing Room 2",
                    BookedBy = "Ching Lee",
                    // BookedDate = DateTime.Today.AddDays(1),
                    BookedTimeFrom = DateTime.Now.AddDays(1).AddHours(1),
                    BookedTimeTill = DateTime.Now.AddDays(1).AddHours(3),
                    BookingDate =  DateTime.Today.AddDays(-2),
                    BuildingId = 1,
                    Id = 2
                },
                new RoomsModel
                {
                    Name = "Football Court",
                    BookedBy = "Pedro Fernandez",
                    // BookedDate = DateTime.Today,
                    BookedTimeFrom = DateTime.Now.AddHours(2),
                    BookedTimeTill = DateTime.Now.AddHours(5),
                    BookingDate =  DateTime.Today.AddDays(-5),
                    BuildingId = 4,
                    Id = 1
                }
            };
            return AllRooms;
        }
    }
}
