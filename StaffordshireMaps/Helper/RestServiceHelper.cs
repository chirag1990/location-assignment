﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using StaffordshireMaps.Models;

namespace StaffordshireMaps.Helper
{
    public static class RestServiceHelper
    {
        //Searching using the Here maps API.
        public async static Task<RootObject> SearchNearestLocation(string latitude, string longitude, string searchString)
        {
            RootObject result = null;
            searchString = searchString.Replace(" ", "%20");
            string url = "https://places.demo.api.here.com/places/v1/discover/search?at=" + latitude + "%2C" + longitude + "&q=" + searchString + "&app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg";
            Debug.WriteLine("The requested url is " + url);
            try
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(new Uri(url));
                if(response.IsSuccessStatusCode)
                {
                    string resultString = await response.Content.ReadAsStringAsync();
                    if(!string.IsNullOrEmpty(resultString))
                    {
                        Debug.WriteLine("The response is \n" + resultString);
                        result = JsonConvert.DeserializeObject<RootObject>(resultString);
                    }
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Failed to get the response for searchNearestLocation " + ex.Message);
            }
            return result;
        }
    }
}
