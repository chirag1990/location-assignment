﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Services.Maps;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using StaffordshireMaps.Helper;
using StaffordshireMaps.Models;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace StaffordshireMaps.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MapView
    {
        ObservableCollection<CustomLocationModel> _universityLocations = new ObservableCollection<CustomLocationModel>();
        ObservableCollection<CustomLocationModel> _searchedLocations = new ObservableCollection<CustomLocationModel>();
        ObservableCollection<CustomLocationModel> _unionOfLocations = new ObservableCollection<CustomLocationModel>();
        ObservableCollection<RoomsModel> _allRooms = new ObservableCollection<RoomsModel>();

        //private Border _previousBorder;
        Geoposition _currentPosition;

        public MapView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            HideStatusBar();
            //Get the initial data for Uni Buildings
            _universityLocations = DataStore.GetBuildingLocations();
            //Get the initial data for Uni Rooms
            _allRooms = DataStore.GetRoomsDetails();
            //Initially the buildings to union of search and uni buildings
            _unionOfLocations = _universityLocations;

            //Set the maps datacontext to bind to unionOfLocations
            myMapControl.DataContext = _unionOfLocations;

            //Setting the map center initially to one of the locations on the uni
            myMapControl.Center = new Geopoint(new BasicGeoposition { Latitude = 52.812227, Longitude = -2.080864 });
        }

        //To hide status bar of the phone
        private async void HideStatusBar()
        {
            StatusBar statusBar = StatusBar.GetForCurrentView();
            // Hide the status bar
            await statusBar.HideAsync();
        }

        //Handles the event where a Uni item on the map is tapped so it can move to Classroom pivot and display the required info
        private void MapIcon_Tapped(object sender, TappedRoutedEventArgs e)
        {
            StackPanel stackPanel = sender as StackPanel;
            if (stackPanel == null) return;
            CustomLocationModel itemTapped = stackPanel.DataContext as CustomLocationModel;
            if (itemTapped != null && itemTapped.Id != -1)
            {
                mainPivot.SelectedIndex = 2;
                StringBuilder result = new StringBuilder();
                IEnumerable<RoomsModel> sampleRooms = _allRooms.Where(x => x.BuildingId == itemTapped.Id);
                if (sampleRooms != null && sampleRooms.Count() > 0)
                {
                    result.AppendLine(itemTapped.Name);
                    result.Append("");
                    foreach (RoomsModel room in sampleRooms)
                    {
                        result.AppendLine("Room : " + room.Name);
                        result.AppendLine("Booked By : " + room.BookedBy);
                        result.AppendLine("Booked On : " + room.BookedTimeFrom.ToString("dd/MM/yyyy"));
                        result.AppendLine("Booking Time From : " + room.BookedTimeFrom.ToString("hh:mm tt"));
                        result.AppendLine("Booking Till Till : " + room.BookedTimeTill.ToString("hh:mm tt"));
                        result.AppendLine("");
                        result.AppendLine("");
                    }
                    roomdetailsTB.Text = result.ToString();
                }
                else
                {
                    roomdetailsTB.Text = "Could not find any details of rooms";
                }
            }
            else
            {
                //roomdetailsTB.Text = "Could not find any details of rooms";
                foreach(UIElement child in stackPanel.Children)
                {
                    if (!(child is Border)) continue;
                    (child as Border).Visibility = (child as Border).Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
                }
            }
            //await ConfirmDirections(itemTapped.Position, itemTapped.Name);
        }

        //Ask the user whether he wants to get the directions with a popup
        private async Task ConfirmDirections(BasicGeoposition position, string placeName)
        {
            try
            {
                MessageDialog msg = new MessageDialog("Do you want to get directions to " + placeName + " from current location?", "Confirm");
                msg.Commands.Add(new UICommand("Yes"));
                IUICommand result = await msg.ShowAsync();
                if (result != null && result.Label.Equals("Yes"))
                {
                    await ShowDirections(position);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Failed to handle confirmation of direcitons " + ex.Message);
            }
        }

        //Show the directions on the map given the destination  and current location as start location
        private async Task ShowDirections(BasicGeoposition destination)
        {
            try
            {
                Geolocator geolocator = new Geolocator();
                _currentPosition = await geolocator.GetGeopositionAsync();

                BasicGeoposition tempCurrent = new BasicGeoposition { Latitude = _currentPosition.Coordinate.Latitude, Longitude = _currentPosition.Coordinate.Longitude };
                //find route
                MapRouteFinderResult result = await MapRouteFinder.GetWalkingRouteAsync(new Geopoint(tempCurrent), new Geopoint(destination));
                myMapControl.Routes.Clear();
                if (result.Status == MapRouteFinderStatus.Success)
                {
                    //put route in a map route view object
                    MapRouteView routeView = new MapRouteView(result.Route);

                    //add route view object to map control
                    myMapControl.Routes.Add(routeView);

                    StringBuilder sb = new StringBuilder();
                    foreach (MapRouteLeg leg in result.Route.Legs)
                    {
                        foreach (MapRouteManeuver maneuver in leg.Maneuvers)
                        {
                            sb.AppendLine(maneuver.InstructionText);
                        }
                    }
                    directionInsTB.Text = sb.ToString();
                }
                else
                {
                    directionInsTB.Text = "No directions found";
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Failed to get the directions " + ex.Message);
            }
        }

        //Gets the current location and then moves the map to that location
        private async void currentLocationBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Geolocator geolocator = new Geolocator();
                _currentPosition = await geolocator.GetGeopositionAsync();

                foreach (MapElement item in myMapControl.MapElements)
                {
                    if (item is MapIcon && (item as MapIcon).Title.Equals("Current Location"))
                    {
                        myMapControl.MapElements.Remove(item);
                        break;
                    }
                }

                MapIcon currentPushpin = new MapIcon();

                currentPushpin.Location = new Geopoint(new BasicGeoposition { Latitude = _currentPosition.Coordinate.Latitude, Longitude = _currentPosition.Coordinate.Longitude });
                currentPushpin.Title = "Current Location";
                Uri myImageUri = new Uri("ms-appx:///Assets/currenticonsmall.png");
                currentPushpin.NormalizedAnchorPoint = new Point(0.5, 0.5);
                currentPushpin.Image = RandomAccessStreamReference.CreateFromUri(myImageUri);
                myMapControl.MapElements.Add(currentPushpin);
                myMapControl.Center = currentPushpin.Location;
                myMapControl.ZoomLevel = 18D;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Failed to get the permission " + ex.Message);
            }
        }

        //Show the seach text box and search button
        private void searchBtn_Click(object sender, RoutedEventArgs e)
        {
            if (searchPanel.Visibility == Visibility.Collapsed)
                searchPanel.Visibility = Visibility.Visible;
            else
                searchPanel.Visibility = Visibility.Collapsed;
        }

        //Showing the commonly used searches (Hospitals,Supermarkets,etc)
        private async void searchNowBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(searchTBox.Text))
            {
                await SearchOnMap(searchTBox.Text);
            }
        }

        //Searches for the text on the map using the Here Maps API and displays it on the map
        private async Task SearchOnMap(string text)
        {
            try
            {
                string address = text;
                Geolocator geolocator = new Geolocator();
                _currentPosition = await geolocator.GetGeopositionAsync();
                _searchedLocations = new ObservableCollection<CustomLocationModel>();
                //Get the result from the Here API
                RootObject result = await RestServiceHelper.SearchNearestLocation(_currentPosition.Coordinate.Latitude.ToString(), _currentPosition.Coordinate.Longitude.ToString(), address);
                if (result != null)
                {
                    foreach (Item item in result.Results.Items)
                    {
                        try
                        {
                            CustomLocationModel newSearchLoc = new CustomLocationModel
                            {
                                Id = -1,
                                IconPath = item.Icon,
                                Name = item.Title,
                                Position = new BasicGeoposition { Latitude = item.Position.ElementAt(0), Longitude = item.Position.ElementAt(1) }
                            };
                            _searchedLocations.Add(newSearchLoc);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("Failed to create the pushpin for search img " + ex.Message);
                        }
                    }
                }
                _unionOfLocations = new ObservableCollection<CustomLocationModel>();
                foreach (CustomLocationModel location in _universityLocations)
                    _unionOfLocations.Add(location);
                foreach (CustomLocationModel location in _searchedLocations)
                    _unionOfLocations.Add(location);
                myMapControl.DataContext = null;
                myMapControl.DataContext = _unionOfLocations;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Failed to search " + ex.Message);
            }
        }

        //Change the map style based on the selected type
        //private void MapStyleBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (MapStyleBox != null)
        //    {
        //        string selectedStyle = ((ComboBoxItem)MapStyleBox.SelectedItem).Content.ToString();

        //        switch (selectedStyle)
        //        {
        //            case "Road":
        //                myMapControl.Style = MapStyle.Road;
        //                break;
        //            case "Aerial":
        //                myMapControl.Style = MapStyle.Aerial;
        //                break;
        //            case "Aerial With Roads":
        //                myMapControl.Style = MapStyle.AerialWithRoads;
        //                break;
        //            case "Terrain":
        //                myMapControl.Style = MapStyle.Terrain;
        //                break;
        //            default:
        //                myMapControl.Style = MapStyle.Road;
        //                break;
        //        }
        //    }
        //}

        //If a map item is held for long ask user whether he wants directions to that location
        private async void StackPanel_Holding(object sender, HoldingRoutedEventArgs e)
        {
            StackPanel stackPanel = sender as StackPanel;
            CustomLocationModel itemHolding = stackPanel?.DataContext as CustomLocationModel;
            if (itemHolding != null) await ConfirmDirections(itemHolding.Position, itemHolding.Name);
        }

        //Zoomin button for map
        private void zoomInBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                myMapControl.ZoomLevel++;
            }
            catch
            {
            }
        }

        //Zoom out button for map
        private void zoomOutBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                myMapControl.ZoomLevel--;
            }
            catch
            {
            }
        }

        //Hide or show the commonly searched items panel
        private void commonSearchItemsBtn_Click(object sender, RoutedEventArgs e)
        {
            ToggleSearchPanel();
        }

        //Hide or show the commonly searched items panel
        private void ToggleSearchPanel()
        {
            if (commonSearchItemsPanel.Visibility == Visibility.Visible)
            {
                commonSearchItemsPanel.Visibility = Visibility.Collapsed;
                //commonSearchButton.Content = "+";
            }
            else
            {
                commonSearchItemsPanel.Visibility = Visibility.Visible;
                //commonSearchButton.Content = "-";
            }
        }


        //Hide or show the commonly searched items panel
        private void ToggleMapStylePanel()
        {
            if (mapStylesItemsPanel.Visibility == Visibility.Visible)
            {
                mapStylesItemsPanel.Visibility = Visibility.Collapsed;
                //mapStyleButton.Content = "+";
                

            }
            else
            {
                mapStylesItemsPanel.Visibility = Visibility.Visible;
                //mapStyleButton.Content = "-";
            }
        }



        //If the compfacility is selected then set its icon as different on the map
        private void compFacility_Click(object sender, RoutedEventArgs e)
        {
            foreach (CustomLocationModel item in _universityLocations)
            {
                if (item.SpecCompFacility)
                {
                    item.IconPath = "ms-appx:///Assets/customIcons/WORLD.png";
                }
            }
        }

        //If the printer is selected then set its icon as different on the map
        private void printers_Click(object sender, RoutedEventArgs e)
        {
            foreach (CustomLocationModel item in _universityLocations)
            {
                if (item.IsPrinterPresent)
                {
                    item.IconPath = "ms-appx:///Assets/customIcons/INFO.png";
                }
            }
        }

        //If the access point is selected then set its icon as different on the map
        private void accessPoints_Click(object sender, RoutedEventArgs e)
        {
            foreach (CustomLocationModel item in _universityLocations)
            {
                if (item.AutomatedAccessPoint)
                {
                    item.IconPath = "ms-appx:///Assets/customIcons/WIFI.png";
                }
            }
        }

        //If the lift is selected then set its icon as different on the map
        private void LiftBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (CustomLocationModel item in _universityLocations)
                {
                    if (item.IsLiftPresent)
                    {
                        item.IconPath = "ms-appx:///Assets/customIcons/STAR.png";
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Failed ::: " + ex.Message);
            }
        }

        //If the common searched item is tapped ie; hospital, supermarkets,etc
        private async void CommonSearchItemTapped(object sender, TappedRoutedEventArgs e)
        {
            string searchQuery = (sender as Border).Tag.ToString();
            ToggleSearchPanel();
            await SearchOnMap(searchQuery);
        }

        private void mapStyleButton_Click(object sender, RoutedEventArgs e)
        {
            ToggleMapStylePanel();
        }

        private void mapStlyle_selected(object sender, TappedRoutedEventArgs e)
        {
            string mapStyleSelected = (sender as Border).Tag.ToString();
            ToggleMapStylePanel();
            if (mapStyleSelected != null)
            {
                switch (mapStyleSelected)
                {
                    case "Road":
                        myMapControl.Style = MapStyle.Road;
                        break;
                    case "Aerial":
                        myMapControl.Style = MapStyle.Aerial;
                        break;
                    case "Aerial With Roads":
                        myMapControl.Style = MapStyle.AerialWithRoads;
                        break;
                    case "Terrain":
                        myMapControl.Style = MapStyle.Terrain;
                        break;
                    default:
                        myMapControl.Style = MapStyle.Road;
                        break;
                }
            }
        }
    }
}
