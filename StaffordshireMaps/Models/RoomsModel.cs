﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace StaffordshireMaps.Models
{
   public class RoomsModel : INotifyPropertyChanged
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set
            {
                if (value != _id)
                {
                    _id = value;
                    RaisePropertyChanged(nameof(Id));
                }
            }
        }

        private int _buildingId;
        public int BuildingId
        {
            get { return _buildingId; }
            set
            {
                if (value != _buildingId)
                {
                    _buildingId = value;
                    RaisePropertyChanged(nameof(BuildingId));
                }
            }
        }


        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                if (value != _name)
                {
                    _name = value;
                    RaisePropertyChanged(nameof(Name));
                }
            }
        }




        private DateTime _bookingDate;
        public DateTime BookingDate
        {
            get { return _bookingDate; }
            set
            {
                if (value != _bookingDate)
                {
                    _bookingDate = value;
                    RaisePropertyChanged(nameof(BookingDate));
                }
            }
        }

        //private DateTime _bookedDate;
        //public DateTime BookedDate
        //{
        //    get { return _bookedDate; }
        //    set
        //    {
        //        if (value != _bookedDate)
        //        {
        //            _bookedDate = value;
        //            RaisePropertyChanged(nameof(BookedDate));
        //        }
        //    }
        //}

        private string _bookedBy;
        public string BookedBy
        {
            get { return _bookedBy; }
            set
            {
                if (value != _bookedBy)
                {
                    _bookedBy = value;
                    RaisePropertyChanged(nameof(BookedBy));
                }
            }
        }

        private DateTime _bookedTimeFrom;
        public DateTime BookedTimeFrom
        {
            get { return _bookedTimeFrom; }
            set
            {
                if (value != _bookedTimeFrom)
                {
                    _bookedTimeFrom = value;
                    RaisePropertyChanged(nameof(BookedTimeFrom));
                }
            }
        }

        private DateTime _bookedTimeTill;
        public DateTime BookedTimeTill
        {
            get { return _bookedTimeTill; }
            set
            {
                if (value != _bookedTimeTill)
                {
                    _bookedTimeTill = value;
                    RaisePropertyChanged(nameof(BookedTimeTill));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged([CallerMemberName]string property = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
