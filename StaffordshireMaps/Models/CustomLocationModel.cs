﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.Devices.Geolocation;

namespace StaffordshireMaps.Models
{
    public class CustomLocationModel : INotifyPropertyChanged
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set
            {
                if (value != _id)
                {
                    _id = value;
                    RaisePropertyChanged(nameof(Id));
                }
            }
        }


        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                if (value != _name)
                {
                    _name = value;
                    RaisePropertyChanged(nameof(Name));
                }
            }
        }


        private BasicGeoposition _position;
        public BasicGeoposition Position
        {
            get { return _position; }
            set
            {
                _position = value;
                RaisePropertyChanged(nameof(Position));
            }
        }

        private bool _isLiftPresent;
        public bool IsLiftPresent
        {
            get { return _isLiftPresent; }
            set
            {
                if (value != _isLiftPresent)
                {
                    _isLiftPresent = value;
                    RaisePropertyChanged(nameof(IsLiftPresent));
                }
            }
        }

        private bool _isPrinterPresent;
        public bool IsPrinterPresent
        {
            get { return _isPrinterPresent; }
            set
            {
                if (value != _isPrinterPresent)
                {
                    _isPrinterPresent = value;
                    RaisePropertyChanged(nameof(IsPrinterPresent));
                }
            }
        }

        private bool _automatedAccessPoint;
        public bool AutomatedAccessPoint
        {
            get { return _automatedAccessPoint; }
            set
            {
                if (value != _automatedAccessPoint)
                {
                    _automatedAccessPoint = value;
                    RaisePropertyChanged(nameof(AutomatedAccessPoint));
                }
            }
        }

        private bool _specCompFacility;
        public bool SpecCompFacility
        {
            get { return _specCompFacility; }
            set
            {
                if (value != _specCompFacility)
                {
                    _specCompFacility = value;
                    RaisePropertyChanged(nameof(SpecCompFacility));
                }
            }
        }

        private string _iconPath;
        public string IconPath
        {
            get { return _iconPath; }
            set
            {
                if (value != _iconPath)
                {
                    _iconPath = value;
                    RaisePropertyChanged(nameof(IconPath));
                }
            }
        }
        private ObservableCollection<RoomsModel> _rooms;

        public ObservableCollection<RoomsModel> Rooms
        {
            get { return _rooms; }
            set
            {
                if (value != _rooms)
                {
                    _rooms = value;
                    RaisePropertyChanged(nameof(Rooms));
                }
            }
        }


        public Geopoint GetGeoPoint => new Geopoint(new BasicGeoposition { Latitude = Position.Latitude, Longitude = Position.Longitude });

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged([CallerMemberName]string property = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
