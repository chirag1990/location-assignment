﻿using System.Collections.Generic;

namespace StaffordshireMaps.Models
{
    public class Category
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Href { get; set; }
        public string Type { get; set; }
        public string System { get; set; }
    }
    public class Tag
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Group { get; set; }
    }

    public class Structured
    {
        public string Start { get; set; }
        public string Duration { get; set; }
        public string Recurrence { get; set; }
    }
    public class OpeningHours
    {
        public string Text { get; set; }
        public string Label { get; set; }
        public bool IsOpen { get; set; }
        public List<Structured> Structured { get; set; }
    }
    public class Item
    {
        public List<double> Position { get; set; }
        public List<double> Bbox { get; set; }
        public int Distance { get; set; }
        public string Title { get; set; }
        public Category Category { get; set; }
        public string Icon { get; set; }
        public string Vicinity { get; set; }
        public List<object> Having { get; set; }
        public string Type { get; set; }
        public string Href { get; set; }
        public string Id { get; set; }
        public double? AverageRating { get; set; }
        public OpeningHours OpeningHours { get; set; }
        public List<Tag> Tags { get; set; }
    }

    public class Results
    {
        public string Next { get; set; }
        public List<Item> Items { get; set; }
    }

    public class Address
    {
        public string Text { get; set; }
        public string House { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string StateCode { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
    }

    public class Location
    {
        public List<double> Position { get; set; }
        public Address Address { get; set; }
    }

    public class Context
    {
        public Location Location { get; set; }
        public string Type { get; set; }
        public string Href { get; set; }
    }

    public class Search
    {
        public Context Context { get; set; }
    }

    public class RootObject
    {
        public Results Results { get; set; }
        public Search Search { get; set; }
    }

}
